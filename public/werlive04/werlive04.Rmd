---
title: "We R Live 04: Manipulando dados raster no R<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Maurício Vancine <br> Felipe Sodré M. Barros <br>"
date: "<br> 26/05/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
library(raster) # raster
library(tmap) # tematic maps
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 04

## Tópicos
### <u><b>Introdução (15 min.)</b></u>
#### 1 Desafio da We R Live 4
#### 2 Pacotes a serem usados
#### 3 Considerações sobre a sintaxe

<br>

--

### <u><b>Mão na massa (30/40 min.)</b></u>
#### 4 Carregar, visualizar e manejar dados matriciais (raster)
#### 5 Reclassificação do raster (Reclassify) 
#### 6 Considerações finais (5 min)

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

### 1. Comentários dos vídeos anteriores: esquecemos, mas respondemos semana passada

- We R Live 01: https://youtu.be/ZORFVdwtJ1U
- We R Live 02: https://youtu.be/eHht0n3Ppcg
- We R Live 03: https://youtu.be/BmlM25XQ3QA

<br>

--

### 2. Agora temos um site para o We R Live:

- Site: https://werlive.netlify.app/

<br>

--

### 3. Dúvidas e sugestões: usem as issues do repositório do GitLab

- GitLab: https://gitlab.com/geocastbrasil/liver

---

class: inverse, middle, center

# 1 Desafio da LiveR 4

---

background-image: url(./img/map_dem_brasil01.png)
background-size: 350px
background-position: 50% 95%

# 1 Desafio da LiveR 4

### Carregar, manejar e visualizar <u><b>dados matriciais (raster)</b></u> no R

### Extras: 
- Pacotes para escalas de cores (**`rcolorbrewer`**, **`viridis`** e **`cptcity`**)
- Mapas raster no **`tmap`**

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

# 2 Pacotes a serem usados

### **`raster`**
https://rspatial.org/raster/

--

### **`rcolorbrewer`**
http://colorbrewer2.org

--

### **`viridis`**
https://cran.r-project.org/web/packages/viridis/vignettes/intro-to-viridis.html

--

### **`cptcity`**
https://ibarraespinosa.github.io/cptcity/

--

### **`tmap`**
https://cran.r-project.org/web/packages/tmap/vignettes/tmap-getstarted.html

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 95%

# 2 Pacotes a serem usados

## Instalação

### Instalar vários pacotes de uma vez
```{r eval=FALSE}
install.packages(c("raster", "rcolorbrewer", "viridis", "cptcity", "tmap"), 
                 dependencies = TRUE)
```

---

class: inverse, middle, center

# 3 Considerações sobre sintaxe

---

background-image: url(./img/raster_concept.png)
background-size: 500px
background-position: 50% 70%

# 3 Considerações sobre sintaxe

## O que é um raster?

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Data Carpentry](https://datacarpentry.org/organization-geospatial/)

---

background-image: url(./img/code_matrix.png)
background-size: 550px
background-position: 60% 95%

# 3 Considerações sobre sintaxe

## Matriz: igual ao ensino médio!!!
Agradeça aos seus professores de matemática!

<br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Data Carpentry](https://datacarpentry.org/organization-geospatial/)

---

background-image: url(./img/single_multi_raster.png)
background-size: 800px
background-position: 50% 45%

# 3 Considerações sobre sintaxe

## Raster vs Brick vs Stack

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Data Carpentry](https://datacarpentry.org/organization-geospatial/)
<br>
Mais: [GeoKrigagem](https://geokrigagem.com.br/geoestatistica-no-r-licao-19-pacote-raster-tutorial/)

---

class: inverse, middle, center

# E que raster vamos utilizar aqui?

---

background-image: url(./img/lidarTree-height.png)
background-size: 400px
background-position: 50% 70%

# 3 Considerações sobre sintaxe

## MDE (Modelo Digital de Elevação)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [Data Carpentry](https://datacarpentry.org/organization-geospatial/)

---

background-image: url(./img/raster_values.gif)
background-size: 600px
background-position: 50% 70%

# 3 Considerações sobre sintaxe

## Acessar valores do raster com indexação
```{r eval=FALSE}
# values
dem[]
```

<br><br><br><br><br><br><br><br><br><br><br><br>

Fonte: [ESRI - Raster basics](https://desktop.arcgis.com/en/arcmap/latest/manage-data/geodatabases/raster-basics.htm)

---

# 3 Considerações sobre sintaxe

## Indexação de vetores
```{r}
vec <- 1:25
vec
```

--

```{r}
vec[1]
vec[1:3]
```

---

# 3 Considerações sobre sintaxe

## Seleção condicional

```{r}
vec>12
```

--

```{r}
vec[vec>12]
```

---

# 3 Considerações sobre sintaxe

## Indexação de matrizes
```{r}
mat <- matrix(1:25, nrow = 5)
mat
```

--

```{r}
mat[1]
mat[1:3]
```

---

# 3 Considerações sobre sintaxe

## Seleção condicional
```{r}
mat>12
```

---

# 3 Considerações sobre sintaxe

## Substituição de valores
```{r}
mat_0 <- mat
mat_0[mat>12] <- 0
mat_0
```

---

# 3 Considerações sobre sintaxe

## Substituição de valores
```{r}
mat_na <- mat
mat_na[mat>12] <- NA
mat_na
```

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive04/werlive04.R`]

---

class: inverse, middle, center

# 6 Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

class: clear
background-image: url(img/logo_werlive.png)
background-size: 400px
background-position: 90% 60%

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)