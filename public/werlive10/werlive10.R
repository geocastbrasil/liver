#' ---
#' title: We R Live 10: Join como associar dados à vetores
#' author: mauricio vancine
#' date: 2020-07-14
#' ---

# prepare r -------------------------------------------------------------
# packages
library(sf) # Simple Features for R, CRAN v0.9-4   
library(tmap) # Thematic Maps, CRAN v3.0 
library(tidyverse) # Easily Install and Load the 'Tidyverse', CRAN v1.3.0
library(geobr) # Loads Shapefiles of Official Spatial Data Sets of Brazil

# import data -------------------------------------------------------------
# coronavirus
# municipality time
cases_brazil_cities_time <- readr::read_csv("https://raw.githubusercontent.com/wcota/covid19br/master/cases-brazil-cities-time.csv")
cases_brazil_cities_time

dplyr::glimpse(cases_brazil_cities_time)

# municipality geodata
mun_geo <- geobr::read_municipality(code_muni = "all", year = 2018)
mun_geo

# map
tm_shape(mun_geo) +
  tm_borders()

ggplot() +
  geom_sf(data = mun_geo) +
  theme_bw()

# Attribute data operations -----------------------------------------------
# Attribute data
sf::st_drop_geometry(mun_geo)

# Vector attribute subsetting
mun_geo_rj <- mun_geo %>% 
  dplyr::filter(abbrev_state == "RJ")
  # dplyr::filter(abbrev_state == "RJ" | abbrev_state == "SP") # & AND e o | OR
  # dplyr::filter(abbrev_state %in% c("RJ", "SP")) # ==, !=, >, =>, <, =<, %in% operacoes logicas
mun_geo_rj

# map
tm_shape(mun_geo_rj) +
  tm_borders()

# create id
mun_geo_rj_id <- mun_geo_rj %>% 
  dplyr::mutate(id = 1:nrow(mun_geo_rj), .after = code_muni)
mun_geo_rj_id

mun_geo_rj_id <- mun_geo_rj %>% 
  dplyr::mutate(id = 1:nrow(mun_geo_rj), .before = 1)
mun_geo_rj_id

# create area
mun_geo_rj <- mun_geo_rj_id %>% 
  dplyr::mutate(area_km2 = sf::st_area(mun_geo_rj)/1e6, .after = 1)
mun_geo_rj

# map
tm_shape(mun_geo_rj) +
  tm_polygons("area_km2") +
  tm_text("name_muni", size = .4)

# Vector attribute joining ------------------------------------------------
# data
dplyr::glimpse(cases_brazil_cities_time)
dplyr::glimpse(mun_geo)

# join
mun_geo_rj_cases <- dplyr::left_join(mun_geo_rj, cases_brazil_cities_time, by = c("code_muni" = "ibgeID"))
mun_geo_rj_cases
dplyr::glimpse(mun_geo_rj_cases)

# subset
mun_geo_rj_cases_rj <- mun_geo_rj_cases %>% 
  dplyr::filter(name_muni == "Rio De Janeiro")
mun_geo_rj_cases_rj

mun_geo_rj_cases_rj_hoje <- mun_geo_rj_cases %>% 
  dplyr::filter(name_muni == "Rio De Janeiro" & date == "2020-07-14")
mun_geo_rj_cases_rj_hoje

tm_shape(mun_geo_rj_cases_rj_hoje) +
  tm_polygons("newCases")

# Vector attribute aggregation --------------------------------------------
# aggregate cases
mun_geo_rj_cases_rj_agg <- mun_geo_rj_cases %>% 
  dplyr::filter(name_muni == "Rio De Janeiro") %>% 
  dplyr::group_by(epi_week) %>% 
  dplyr::summarise(casos = sum(newCases, na.rm = TRUE))
mun_geo_rj_cases_rj_agg

# map
tm_shape(mun_geo_rj_cases_rj_agg) +
  tm_polygons("casos", legend.show = FALSE) +
  tm_text("casos", size = 1.5) +
  tm_facets("epi_week")

# Spatial joining ---------------------------------------------------------
# centroids
mun_geo_rj_cen <- sf::st_centroid(mun_geo_rj, of_largest_polygon = TRUE) # 
mun_geo_rj_cen

# map
tm_shape(mun_geo_rj) +
  tm_borders() +
tm_shape(mun_geo_rj_cen) +
  tm_bubbles(size = .2)

# spatial joining
mun_geo_rj_cen_join <- sf::st_join(mun_geo_rj_cen, mun_geo_rj_cases)
mun_geo_rj_cen_join

# aggregate cases
mun_geo_rj_cen_join_agg <- mun_geo_rj_cen_join %>% 
  tidyr::drop_na(epi_week) %>% 
  dplyr::group_by(epi_week, name_muni.x) %>% 
  dplyr::summarise(casos = sum(newCases, na.rm = TRUE)) %>% 
  dplyr::ungroup()
mun_geo_rj_cen_join_agg

# map casos
map_cent <- tm_shape(mun_geo_rj, bbox = mun_geo_rj) +
  tm_borders() +
  tm_shape(mun_geo_rj_cen_join_agg) +
  tm_bubbles(size = "casos", col = "red", border.col = "gray", 
             contrast = 1, scale = 4) +
  tm_facets("epi_week", nrow = 1, ncol = 1) +
  tm_layout(legend.show = FALSE)
map_cent

# animation
tmap_animation(map_cent, width = 800, delay = 40)

# end ---------------------------------------------------------------------