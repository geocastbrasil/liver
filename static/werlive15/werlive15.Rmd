---
title: "We R Live 15: Introdução à estatística espacial V<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Felipe Sodré M. Barros <br>Maurício Vancine <br>"
date: "<br> 15/09/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 15

## Tópicos
#### <u><b>Introdução (40 min.)</b></u>
#### 1 Desafio da We R Live 15
#### 2 Pacotes a serem usados
#### 3 Considerações conceituais  

  * Revisão correlação espacial  
  * Indice Local de Correlação Espacial  
  *"Local indicators of spatial association" (LISA)*  

#### Mão na massa (30/40 min.)

#### 4 Considerações finais (5 min)

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

<br>

### 1. Apoie as iniciativas do GeoCast Brasil:
- Não deixe de curtir as *lives* e videos;
- Não deixe de se inscrever no canal;
- Ajude divulgando nas redes sociais;

### 2. Lives passadas: visite nosso site:
- Site: https://werlive.netlify.app/

### 3. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

<br>
.center[
[`r icon::fa_gitlab(size = 2)`](https://gitlab.com/geocastbrasil/liver)
[`r icon::fa_twitter(size = 2)`](https://twitter.com/GeoCastBrasil)
[`r icon::fa_instagram(size = 2)`](https://www.instagram.com/geocastbrasil/)
[`r icon::fa_facebook(size = 2)`](https://facebook.com/GeoCastBrasil/)
[`r icon::fa_telegram(size = 2)`](https://t.me/GeoCastBrasil)]

---

background-image: url(img/werlive12.png), url(img/werlive13.png)
background-size: 400px,400px
background-position: 10% 50%,90% 50%

# Recados

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
[We R Live 12](https://www.youtube.com/watch?v=KN4kkvKkb_w)

[We R Live 13](https://www.youtube.com/watch?v=IsOJvaWdyXE)

---

# Recados

## II Simpósio Brasileiro de Infraestrutura de Dados Espaciais
### Resumo aceito: "Ensino e gestão do conhecimento em geotecnologias para um “novo normal”: reflexões a partir das iniciativas do GeoCast Brasil"

<br>

```{r, echo=FALSE, out.width = "450px", fig.align="center"}
knitr::include_graphics("./img/sbide.png")
```

https://inde.gov.br/simposio-12-anos/sbide-home.html

---

# Recados

## We R Live Coding 02 - On fire!
### Convidado: João Giovanelli
<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlivecoding02.png")
```

---

# Recados

## R Markdown: usando o R para comunicar seus resultados  
### Convidada: Beatriz Milz

<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlive16.png")
```

---

# Recados

## Série de Modelos de Nicho Ecológico
### Convidadas: Andrea Sánchez-Tapia e Sara Mortara

<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlive17.png")
```

---

class: inverse, middle, center

# 1 Desafio da We R Live 15

---

# 1 Desafio da We R Live 15

### Identificar os municípios que apresentam correlação espacial nos votos para presidente de 2018  
- Análise local de correlação espacial dos votos;  

--

### Extras: 
- Mapa de rede de vizinhança com **Tmap**
- Usar funções do *tidyverse*, como:
  - `group_by()`
  - `summarize()`
- Acessar dados das eleições com pacote [Cepesp-Fgv](https://github.com/Cepesp-Fgv/cepesp-r)  

já explorados nas [Live 13](https://gitlab.com/geocastbrasil/liver/-/tree/master/static/werlive13)
e [Live 10](https://gitlab.com/geocastbrasil/liver/-/tree/master/static/werlive10)

---

background-image: url(img/tmap_neighbours.png), url(img/LISA.png)
background-size: 400px,400px
background-position: 10% 50%,90% 50%

# 1 Desafio da We R Live 15

---
background-image: url(img/livro_rstats.jpeg) 
background-size: 250px
background-position: 90% 50%

# 1 Desafio da We R Live 15

### Base conceitual  

* ["Análise Espacial de Dados Geográficos"](http://www.dpi.inpe.br/gilberto/livro/analise/cap5-areas.pdf)  

* [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)  

* [Crime Mapping in R - Juanjo Medina and Reka Solymosi](https://maczokni.github.io/crimemapping_textbook_bookdown/global-and-local-spatial-autocorrelation.html#morans-i)  

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 90% 90%

# 2 Pacotes a serem usados

### **`sf`**
[github](https://r-spatial.github.io/sf/index.html)

### **`dplyr`**
[cran](https://cran.r-project.org/web/packages/dplyr/)

### **`tmap`**
[github](https://github.com/mtennekes/tmap)

### **`geobr`**
[github](https://github.com/ipeaGIT/geobr)

### **`spdep`**
[cran](https://mran.revolutionanalytics.com/snapshot/2020-04-25/web/packages/spdep/index.html)

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 95%

# 2 Pacotes a serem usados

## Instalação

### Instalar pacotes

```{r eval=FALSE}
install.packages(c("sf", "tidyverse", "tmap", "geobr", "spdep"), 
                 dependencies = TRUE)
```

---

class: inverse, middle, center

# 3 Considerações conceituais

---

# 3 Considerações conceituais

## Revisão correlação espacial  

A anáise global de correlação espacial estima o valor de correlação espacial para toda a área de estudo. 

Logo, prezume homogeneidade em toda área de estudo.

Como são raros os casos de homogeineidade, estimar o valor global, apenas, não é suficiente.

Precisamos de uma estatística local para poder identificar, caso haja, a variação espacial interna à área.

---

# 3 Considerações conceituais

### Por que isso importa?

Identificar a existencia de correlação espacial e sua variação local é importante pois:
- ao existir, a premissa de observações idependentes é violada
- e com isso, um modelo estatístico criado com dados correlacionados espacialmente apresentarão estimativas viezadas e inadequadas

Em resumo, quando a correlação epsacial é observada, podemos dizer que o valor de uma área poderiam ser explicada (e predita) a partir do valor das áreas vizinhas.

> Encontrar a correlação espacial, seja global ou local, não é um fim em sí, mas uma etapa necessária para alcançar um modelo estatístico mais proprício.

---
background-image: url(./img/nc_spdep.png),url(./img/ecgraphy_enm.png)
background-size: 450px,400px
background-position: 1% 80%,99% 90%

# 3 Considerações conceituais
### Por que isso importa?
[link: Nature Comunications](https://www.nature.com/articles/s41467-020-18321-y.pdf)
[link: Ecography](https://onlinelibrary.wiley.com/doi/epdf/10.1111/ecog.05134)

---
# 3 Considerações conceituais

## Revisão correlação espacial  

### Disclaimer

> `r icon::fa_exclamation_triangle()` Os resultados de correlação espacial local não devem ser vistos como "absolutos", mas como condicionados pela autocorrelação espacial global e, de forma mais geral, pela possível influência dos processos de geração de dados espaciais em uma gama de escalas do global ao local até dependência não detectada na escala das observações.

---

# 3 Considerações conceituais

## Revisão correlação espacial  

Vimos na última live que o teste global de Moran-I é o coeficiente angular da relação linear entre a variável de análise e a sua média móvel (considerando os vizinhos definidos).

```{r, message=FALSE, warning=FALSE, echo=FALSE, prompt=FALSE, results='hide', out.width = "400px"}
library(tmap)
library(dplyr)
library(sf)
library(spdep)
library(readr)
presidente <- read_csv("./data/eleicoes_2018.csv")
municipios <- read_sf("./data/RJ_municipios.shp")
municipios <- municipios %>% left_join(presidente, by = c("code_mn" = "COD_MUN_IBGE"))

municipios <- municipios %>% 
  group_by(code_mn) %>% 
  mutate(
  total = sum(QTDE_VOTOS),
  perc_vote = round((QTDE_VOTOS * 100) / total, 2)
)

pt <- municipios %>% group_by(code_mn) %>%
  filter(NUMERO_CANDIDATO == 13)

nb <- poly2nb(pt, queen=TRUE, row.names = pt$name_mn) 

lw <- nb2listw(nb, style="W", zero.policy=TRUE) 

pt$lag <- lag.listw(lw, pt$perc_vote)

# lag <- tm_shape(pt) + 
#   tm_polygons(col = "lag", style = "fisher", title = "media movel")
# perc <- tm_shape(pt) + 
#   tm_polygons(col = "perc_vote", style = "fisher", title = "% votos")
# 
# tmap_arrange(perc, lag, ncol = 2)
```

```{r}
regressao <- lm(pt$lag ~pt$perc_vote)
coef( regressao )
```

---
# 3 Considerações conceituais

## Revisão correlação espacial  

Vimos na última live que o teste global de Moran-I é o coeficiente angular da relação linear entre a variável de análise e a sua média móvel (considerando os vizinhos definidos).

```{r}
moran.test(pt$perc_vote, lw)
```

---

# 3 Considerações conceituais

## Revisão correlação espacial  

Se colocarmos isso em um gráfico de dispersão, poderíamos particionar o mesmo em quandrantes a apartir do valor médio da variável.

```{r, out.width = "350px"}
plot(pt$perc_vote, pt$lag)
abline(regressao, col = "red")
```

---

# 3 Considerações conceituais

## Revisão correlação espacial  

Isso pode ser feito facilmente com a função `moran.plot()`:

```{r, out.width = "400px"}

moran.plot(pt$perc_vote, listw = lw)

```

---

# 3 Considerações conceituais

## Indice Local de Correlação Espacial  

Nesse gáfico de dispersão podemos identificar as áreas que estão:

```{r, echo=FALSE, out.width = "650px"}
knitr::include_graphics("./img/scaterplot.png")
```


---

# 3 Considerações conceituais

## Indice Local de Correlação Espacial  

Valores próximos à média indicam aglomeração de valores similares em sua vizinhança (correlação espacial positiva). Valores que se afastam da média indicam *hotspots* onde os valores contíguos diferem mais do que sería esperado (correlação espacial negativa). 

---

# 3 Considerações conceituais

## Indice Local de Correlação Espacial  

O interessante é que o `moran.plot()`, usa uma função secundária (`influence.measures()`) que permite identificar se uma área em particular é capaz de influenciar o coeficiente de declividade mais do que o esperado.

Mais infos? Só estudando mesmo `r icon::fa_sad_cry()`

---


# 3 Considerações conceituais

## Indice Local de Correlação Espacial  

Para facilitar a interpretação, vamos usar a função `scale()` para que os valores sejam rescalonados e a média fique em zero. mais infos: `?scale()`

```{r, out.width = "350px"}
pt$s_vote <- scale(pt$perc_vote) %>% as.vector()
moran.plot(pt$s_vote, listw = lw)
```

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---

# Script para essa live

<br><br><br><br><br><br><br>

#### .center[https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive15/werlive15.R]

---

class: inverse, middle, center

# Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

# Próxima Live Coding

## We R Live Coding 02 - On fire!
### Convidado: João Giovanelli

<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlivecoding02.png")
```

---

# Próxima Live

## R Markdown: usando o R para comunicar seus resultados  
### Convidada: Beatriz Milz

<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlive16.png")
```

---

# Próximas Lives

## Série de Modelos de Nicho Ecológico
### Convidadas: Andrea Sánchez-Tapia e Sara Mortara

<br>

```{r, echo=FALSE, out.width = "650px", fig.align="center"}
knitr::include_graphics("./img/werlive17.png")
```

---

class: clear

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré M. Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)