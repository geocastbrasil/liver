# loading packages ----
library(tmap)
library(dplyr)
library(sf)
library(spdep)
library(readr)

# Loading data ----
presidente <- read_csv("./data/eleicoes_2018.csv")
municipios <- read_sf("./data/RJ_municipios.shp")
municipios <- municipios %>% left_join(presidente, by = c("code_mn" = "COD_MUN_IBGE"))

# calculating % of votes:
municipios <- municipios %>% 
  group_by(code_mn) %>% 
  mutate(
    total = sum(QTDE_VOTOS),
    perc_vote = round((QTDE_VOTOS * 100) / total, 2)
  ) %>% select(NUMERO_CANDIDATO, perc_vote, total, code_mn, name_mn)

# map ----
pt <- municipios %>% group_by(code_mn) %>%
  filter(NUMERO_CANDIDATO == 13)

# Vizinhanca ----
# lista de vizinhanca usando "queen case"
nb <- poly2nb(pt, queen=TRUE, row.names = pt$name_mn) 

# peso a lista de vizinhanca ----
lw <- nb2listw(nb, style="W", zero.policy=TRUE) 

# mapa con % votos:
perc <- tm_shape(pt) + 
  tm_polygons(col = "perc_vote", style = "fisher", title = "% votos") +
  tm_grid(lwd = 0)

# mapa de vizinhança com tmap ---
# Identificando centroides
centroides <- st_centroid(st_geometry(pt))
# convertendo Vizinhanca apenas
?nb2lines
lines <- nb2lines(nb = nb, coords = centroides, as_sf = TRUE)
lines

# Mapa vizinhanca
tm_shape(municipios) +
  tm_borders(col = 'grey') +
  tm_shape(lines)+
  tm_lines(lty = 'dashed') +
  tm_shape(centroides) + 
  tm_dots(col = "red", size = .1)

# convertendo vizinhança com peso
?listw2lines
linesw <- listw2lines(lw, coords = centroides, as_sf = TRUE)
# Mapa de vizinhanca com peso
nbmap <- tm_shape(municipios) +
  tm_borders(col = 'grey') +
  tm_shape(linesw) +
  tm_lines(lty = 'dashed', lwd = 'wt', scale = 4) +
  tm_shape(centroides) + 
  tm_dots(col = "red", size = .1) + 
  tm_grid(lwd = 0)
# tmap_save(nbmap, "./img/tmap_neighbours.png")

# mapa com layouts produzidos
tmap_arrange(nbmap, perc)

# Indice Global Moran ----
MC <- moran.mc(pt$perc_vote, lw, nsim=100) # permutation for Moran I

# View results (including p-value)
MC

# Plot the distribution (note that this is a density plot instead of a histogram)
plot(MC)

# Reescalonando os valores de votos
pt$s_vote <- scale(pt$perc_vote) %>% as.vector()

# Grafico de dispersao
?moran.plot
moran.plot(pt$s_vote, lw)

# Moran Local ------
?localmoran

localm <- localmoran(pt$s_vote, lw)
localm <- localm %>% as.data.frame() %>% as_tibble()

localm
localm %>% summary()

# Juntando resultado moran local com dataset pt
glimpse(pt)
pt <- bind_cols(pt, localm)

# mapa com o moran-I local ---
moranLocal <- tm_shape(pt) +
  tm_polygons("Ii", title = "Moran-I", style = "fisher") +
  tm_grid(lwd = 0)

tmap_arrange(nbmap, perc, moranLocal)

# Identificando apenas os significativos
glimpse(pt)
pt <- pt %>% 
  mutate(sig = ifelse(`Pr(z > 0)` <= 0.05, 
                      Ii, NA))

moranSig <- tm_shape(pt) +
  tm_polygons("sig", title = "Moran-I", textNA = "Não significativo") +
  tm_grid(lwd = 0)

tmap_arrange(nbmap, perc, moranLocal, moranSig)

# transformando em mapa categorico
pt$s_lag <- lag.listw(lw, pt$s_vote)

glimpse(pt)
glimpse(pt)
pt <- pt %>% 
  mutate(quad_sig = ifelse(s_vote > 0 & 
                             s_lag > 0 & 
                             `Pr(z > 0)` <= 0.05, 
                           "high-high",
                           ifelse(s_vote <= 0 & 
                                    s_lag <= 0 & 
                                    `Pr(z > 0)` <= 0.05, 
                                  "low-low", 
                                  ifelse(s_vote > 0 & 
                                           s_lag <= 0 & 
                                           `Pr(z > 0)` <= 0.05, 
                                         "high-low",
                                         ifelse(s_vote <= 0 & 
                                                  s_lag > 0 & 
                                                  `Pr(z > 0)` <= 0.05,
                                                "low-high", 
                                                "non-significant")))))


# Analizando a quantidade de áreas em cada classe
?table
table(pt$quad_sig)
pt %>% st_drop_geometry() %>% count(quad_sig)

# mapa categorico de correlação espacial
lisa <- 
  tm_shape(pt) + 
  tm_polygons(col = "quad_sig", title = "LISA") +
  tm_grid(lwd = 0)

tmap_arrange(nbmap, perc, moranLocal, moranSig, lisa)

# criando uma variavel que recebera o nome do municipio quando o valor for significativo
pt <- pt %>%
  mutate(
    label = ifelse(quad_sig == "high-high" |
                  quad_sig == "low-low", name_mn, ' '))

# Resultado do desafio do Mauricio:
pt <- pt %>%
  mutate(
    label = ifelse(`Pr(z > 0)` <= 0.05,
                   name_mn, ''))

# Fazendo um novo mapa com nome dos municipios que resultaram significativos
lisa <- 
  tm_shape(pt) + 
  tm_polygons(col = "quad_sig", title = "LISA") +
  tm_text("label", size = .7) + 
  tm_grid(lwd = 0)
# tmap_save(lisa, "./img/LISA.png")
tmap_arrange(nbmap, perc, moranLocal, moranSig, lisa)

# end------