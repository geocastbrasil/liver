---
title: "We R Live 13: Introdução à estatística espacial IV<br><br>"
subtitle: "<br>GeoCast Brasil" 
author: "Felipe Sodré M. Barros <br>Maurício Vancine <br>"
date: "<br> 18/08/2020"
output:
  xaringan::moon_reader:
    css: [metropolis]
    lib_dir: libs
    nature:
      highlightStyle: rainbow
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, encoding = "UTF-8")
```

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 13

## Tópicos
### <u><b>Introdução (40 min.)</b></u>
#### 1 Desafio da We R Live 13
#### 2 Pacotes a serem usados
#### 3 Considerações conceituais  

  * Dados agregados ou dados de área  
  * Alguns cuidados  
  * Análise exploratória  
  * Teste de correlação espacial (Indice de Moran I)
    - 3.1 O que é
    - 3.2 Vizinhança
    - 3.3 Peso espacial
    - 3.4 Lagging value (média móvel)
    - 3.5 Cálculo Indice Global de Moran I

---

background-image: url(img/logo_werlive.png)
background-size: 250px
background-position: 95% 30%

# We R Live 13

#### 4 Processamento de dados  
  * `group_by()`  
  * `summarise()`  

#### Mão na massa (30/40 min.)

#### O pulo do gato

#### 5 Considerações finais (5 min)

---

class: inverse, middle, center

# Mas antes! Recados!!!

---

# Recados

<br>

### 1. Apoie as iniciativas do GeoCast Brasil:
- Não deixe de curtir as *lives* e videos;
- Não deixe de se inscrever no canal;
- Ajude divulgando nas redes sociais;

### 2. Lives passadas: visite nosso site:
- Site: https://werlive.netlify.app/

### 3. Dúvidas e sugestões: issues
- GitLab: https://gitlab.com/geocastbrasil/liver/-/issues

<br>
.center[
[`r icon::fa_gitlab(size = 2)`](https://gitlab.com/geocastbrasil/liver)
[`r icon::fa_twitter(size = 2)`](https://twitter.com/GeoCastBrasil)
[`r icon::fa_instagram(size = 2)`](https://www.instagram.com/geocastbrasil/)
[`r icon::fa_facebook(size = 2)`](https://facebook.com/GeoCastBrasil/)
[`r icon::fa_telegram(size = 2)`](https://t.me/GeoCastBrasil)]

---

background-image: url(img/GeografiaEleicoes.png)
background-size: 600px
background-position: 50% 50%

# Recados

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
[`r icon::fa_youtube(size = 2)`](https://werlive.netlify.app/blog/werlive12/)
[`r icon::fa_link(size = 1)`  Slides](https://lgelape.github.io/blog/geografia_eleicoes/geografia_eleitoral_slides.pdf)

---

class: inverse, middle, center

# 1 Desafio da We R Live 13

---

# 1 Desafio da We R Live 13

### Analisar estrutura de distribuição espacial dos votos no estado do RJ nas eleições de 2018  
- Análise/teste de correlação espacial dos votos;  

--

### Extras: 
- Acessar dados da eleição de 2018;
- Usar funções do *tidyverse*, como:
  - `group_by()`
  - `summarize()`
- Realizar *joins* tabulares e espaciais (já explorado na [live 10](https://gitlab.com/geocastbrasil/liver/-/tree/master/static/werlive10))

--

background-image: url(img/Votos.png),url(img/neighbours.png)
background-size: 400px,425px
background-position: 85% 25%,85% 135%

---
background-image: url(img/livro_rstats.jpeg) 
background-size: 250px
background-position: 100% 50%

# 1 Desafio da We R Live 13

### Base conceitual  

* ["Análise Espacial de Dados Geográficos"](http://www.dpi.inpe.br/gilberto/livro/analise/cap5-areas.pdf)  

* [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)  

* [Intro to GIS and Spatial Analysis - Manuel Gimond](https://mgimond.github.io/Spatial/spatial-autocorrelation.html)  

* [Crime Mapping in R - Juanjo Medina and Reka Solymosi](https://maczokni.github.io/crimemapping_textbook_bookdown/global-and-local-spatial-autocorrelation.html#morans-i)  

---

class: inverse, middle, center

# 2 Pacotes a serem usados

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 70%

# 2 Pacotes a serem usados

### **`sf`**
[github](https://r-spatial.github.io/sf/index.html)

### **`dplyr`**
[cran](https://cran.r-project.org/web/packages/dplyr/)

### **`tmap`**
[github](https://github.com/mtennekes/tmap)

### **`geobr`**
[github](https://github.com/ipeaGIT/geobr)

### **`spdep`**
[cran](https://mran.revolutionanalytics.com/snapshot/2020-04-25/web/packages/spdep/index.html)

### **`cepespR`**
[github](https://github.com/Cepesp-Fgv/cepesp-r)

---

background-image: url(./img/package_r.png)
background-size: 250px
background-position: 50% 95%

# 2 Pacotes a serem usados

## Instalação

### Instalar pacotes

```{r eval=FALSE}
install.packages(c("sf", "tidyverse", "tmap", "geobr", "spdep", "cepespR"), 
                 dependencies = TRUE)
```

--

```{r eval=FALSE}
# Para instalar o cepesp-r - dados das eleicoes
if (!require("devtools")) install.packages("devtools")
devtools::install_github("Cepesp-Fgv/cepesp-r")
```

---

class: inverse, middle, center

# 3 Considerações conceituais

---

# 3 Considerações conceituais

## Dados agregados ou dados de área

Os dados aglomerados ou dados de área (os famosos dados poligonais) possuem informações a respeito de uma determinada área do espaço geográfico.  

Os limites desses poligonos podem ser definidos tanto pelo investigador (levando em consideranção o objeto de estudo), como um limite arbitrário ou, até mesmo, um limite administrativo criado com diferentes objetivos. Neles as informações associadas são frequentemente agregadas como, por exemplo, contagem populacional.

Nesses casos, não se pode saber exatamente como se dá a distribuição espacial do fenómeno estudado dentro do limite que o agrega.

---

# 3 Considerações conceituais

## Alguns cuidados

A coleta de dados deve considerar a área de análise. Dessa forma, a inlfuencia do da escala será reduzida.

E quando se trata de análise temporal temos que tomar cuidado com o fato de que as áreas poderão ter sido alteradas com o passar do tempo. Para solucionar isso, se usa as **áreas mínimas comparáveis (AMCs)**.

Por exemplo:

--

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "400px", fig.align='center'}

knitr::include_graphics("./img/MAUP.gif")
```

---

# 3 Considerações conceituais

## Alguns cuidados

A coleta de dados deve considerar a área de análise. Dessa forma, a inlfuencia do da escala será reduzida.

E quando se trata de análise temporal temos que tomar cuidado com o fato de que as áreas poderão ter sido alteradas com o passar do tempo. Para solucionar isso, se usa as **áreas mínimas comparáveis (AMCs)**.

Por exemplo:

`r icon::fa_exclamation_triangle()` O **IPEA** está trabalhando na elaboração desses dados para o Brasil e em breve estará disponível pelo [geobr](https://github.com/ipeaGIT/geobr). `r icon::fa_thumbs_up()`

---

# 3 Considerações conceituais

## Análise exploratória

A forma mais usual de explorar este tipo de dado é representando os mesmos em forma de mapas [coropléticos](https://pt.wikipedia.org/wiki/Mapa_coropl%C3%A9tico) (a.k.a "fazendo mapas bonitinhos"): Mapas coloridos representando a variação de determinado **valor** ou **categoría** em uma área de estudo que contém várias áreas amostrais (polígonos).

E é comum usarmos este tipo de mapa para tentar tirar conclusões sobre o **padrão espacial do fenômeno**.  

Trata-se, como alerta [Câmara et al.](http://www.dpi.inpe.br/gilberto/livro/analise/cap5-areas.pdf), de uma abordagem meramente intuitivas/subjetiva. E que, para alguns casos, precisamos ir além.

---

# 3 Considerações conceituais

## Teste de correlação espacial

Discernir **visualmente** o padrão de distribuição de determinado fenômeno **não é uma tarefa óbvia**, além de ter uma carga subjetiva elevada.  

O teste de autocorrelação espacial se apresenta como uma abordagem estatística (quantitativa e objetiva) para identificar qual o padrão de distribuição espacial das informações relacionadas aos polígonos.  

Vamos usar o **Indice de Moran I** (Moran's I test).

`r icon::fa_exclamation_triangle()` [Para saber mais sobre os padrões de distribuição espaciais: (re)vejam a live #7](https://werlive.netlify.app/blog/werlive07/).

---
# 3 Considerações conceituais

## Indice de Moran I

### 3.1 O que é

O indice de Moran I é um coeficiente que informa se o valor de uma variável (como quantidade de votos para um candidato, riqueza de espécies, quantidade de pessoas afetadas por um virus, etc) em uma feição  é correlacionada com os valores das feições vizinhas.

Com o indice global de Moran I, se tem a magnitude da autocorrelação espacial da variável estudada entre as áreas amostradas.

--

Mas antes de realizar o tete, temos que pensar e discutir alguns conceitos fundamentais, como:
1. Vizinhança;
2. Peso espacial;

---

# 3 Considerações conceituais

## 3.2 Vizinhança  

A definição de vizinhança é fundamental nesse tipo de análise, e deve ser considerado a natureza do fenômeno de estudo na definição do mesmo, já que há várias formas de definir os polígonos vizinhos:

1. Por contiguidade;  
  - Queen case;  
  - Rook case;  
--

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "400px", fig.align='center'}

knitr::include_graphics("./img/neighbourscase.png")
```

---

# 3 Considerações conceituais

## 3.2 Vizinhança  

A definição de vizinhança é fundamental nesse tipo de análise, e deve ser considerado a natureza do fenômeno de estudo na definição do mesmo, já que há várias formas de definir os polígonos vizinhos:

1. Por contiguidade;  
  - Queen case;  
  - Rook case;  
--

2. Por distância;  
3. *K* Vizinhos mais próximos (*k nearest neighbors*);  

`r icon::fa_exclamation_triangle()` Os métodos de vizinhança baseados em distância, em geral, **usam o centroide do polígono** para a estimação dos polígonos vizinhos **e não o seu perímetro/vértices**.

---

# 3 Considerações conceituais

## 3.2 Vizinhança

### Contiguidade  

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "800px", fig.align='center'}

knitr::include_graphics("./img/contiguidade.png")
```

Fonte: [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)  

---

# 3 Considerações conceituais

## 3.2 Vizinhança

### Distância  

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "800px", fig.align='center'}

knitr::include_graphics("./img/distancia.png")
```

Fonte: [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)  

---

# 3 Considerações conceituais

## 3.2 Vizinhança

### K vizinhos mais próximos

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "800px", fig.align='center'}

knitr::include_graphics("./img/k_vizinhos.png")
```

Fonte: [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)  

---

# 3 Considerações conceituais

## 3.2 Vizinhança  

Uma vez definido o conceito de vizinhança a ser utilizado, pode-se construir a matriz de vizinhança, também chamada matriz de proximidade espacial ( $w_{ij}$ ). Que estão será usada na análise de correlação espacial.

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "400px", fig.align='center'}

knitr::include_graphics("./img/MatrizVizinhanca.png")
```

Fonte: [Adaptado de *Câmara et al.*](http://www.dpi.inpe.br/gilberto/livro/analise/cap5-areas.pdf)  

`r icon::fa_exclamation_triangle()` Há a possibilidade de considerar vizinhança de segunda ou maiores ordens.  

`r icon::fa_exclamation_triangle()` Um ponto a considerar é com relação aos polígonos que estejam no limite da área de estudo que, em geral terão menos polígonos vizinhos e com isso induzir a um viés de **super** ou **sub estimação** da correlação espacial.

---

# 3 Considerações conceituais

## 3.3 Peso espacial

Antes de seguir com a análise, precisamos definir o **peso espacial** ao qual podemos adotar algumas estratégias:

1. Definir o mesmo peso a cada polígonos (style = "W") (mais intuitivo);  
  - O valor será definido pela fração $\frac{1}{\# vizinhos}$;
  - A cada vizinho é atribuido o valor ponderado pelo peso;
2. Outras opções sariam:
  - "B" definindo um valor binário para vizinhos e não vizinhos;  
  - Ver mais em `?nb2listw`

`zero.policy = TRUE`: peso igual a zero é inserido para regiões sem vizinhos.

---

# 3 Considerações conceituais

## 3.3 Peso espacial

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "600px", fig.align='center'}

knitr::include_graphics("./img/PesoEspacial.png")
```
Fonte: [*Câmara et al.*](http://www.dpi.inpe.br/gilberto/livro/analise/cap5-areas.pdf)  

---
# 3 Considerações conceituais

## 3.4 *Lagging value* (média móvel)

Feita a identificação dos polígonos vizinhos e a definição do peso espacial, calcula-se um resumo dos valores dos polígonos vizinhos aglomerados (calculando, por exemplo seus valores médios), chamado de *lagging value* ou **média móvel**:  

$\hat\mu_{i} = \displaystyle\sum_{j=1}^{n}w_{ij}z_{i}$

> No nosso trabalho, vamos adotar a estimativa de vizinhança por contiguidade e o peso ponderado pela quantidade de vizinhos ("W"), usando o percentual de votos para o PT como variável de análise.

---

# 3 Considerações conceituais

## 3.5 Cálculo Indice Global de Moran I

$I = \frac{ \displaystyle\sum^{n}_{i=1}\sum^{n}_{j=1} w_{ij} (z_{i} - \bar{z})(z_{j} - \bar{z})}{\displaystyle\sum^{n}_{i=1} (z_{i} - \bar{z})^2}$

O indice de Moran I é um coeficiente que varia entre valores negativos e positivos (entre -1 e +1) indicando desde correlação inversa até correlação direta, sendo os valores zero a ausencia de correlação espacial.

Contudo, independente do resultado, precisamos identificar o nível de significância do mesmo.

---

# 3 Considerações conceituais

## 3.6 Cálculo de sginificância

O indice de Moran I, por si só, nos informa o valor obervado de correlação espacial. Para confirmar que este valor é significativo, devemos compará-lo com valores simulados segundo a hipótese nula (distribuição aleatória).

Para isso, vamos usar a simulação de Monte Carlo que usa os mesmos polígonos, ams atribui a eles valores aleatórios clculando o indice Moran I a cada simulação. Com isso, temos a distribuição dos valores de Moran I segundo a hipótese nula.

Essa dsitribuição é então comparada com o valor observado do teste Moran I.

---

class: inverse, middle, center

# Processamento de dados `r icon::fa_laptop_code()`

---

# 4 Processamento de dados

Faremos o download dos dados das eleições pelo pacote `cepespR`. Nele teremos a cada municipio do Rio de Janeiro, dois valores: Total de votos para o candidato do PT (Fernando Haddad) e o total de votos ao candidato do PSL ().  

Vamos filtrar para termos os dados do segundo turno, apenas. Além disso, precisaremos fazer alguns processamentos para poder trabalhar com o percentual de votos. Para isso usaremos duas funções muito importantes e úteis: `group_by()` e `summarise()`:
---

# 4 Processamento de dados

## `group_by()`

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "600px", fig.align='center'}

knitr::include_graphics("./img/groupby.png")
```

---

# 4 Processamento de dados

## `summarise()`

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "600px", fig.align='center'}

knitr::include_graphics("./img/groupbySummarise.png")
```

Essas funoes serão usadas para calcular o total de votos válidos de cada município e, em seguida, calcular o percentual de votos a cada candidato.

---

class: inverse, middle, center

# Mão na massa!!! `r icon::fa_laptop_code()`

---
background-image: url(./img/puloDoGato.jpeg)
background-size: 500px
background-position: 50% 90%

class: inverse, middle, center

# O Pulo do gato 



---


# O Pulo do gato

## O que é de fato o Moran I?  

```{r, message=FALSE, warning=FALSE, echo=FALSE, prompt=FALSE, results='hide'}
library(cepespR)
library(geobr)
library(tmap)
library(dplyr)
library(sf)
library(spdep)
presidente <- get_votes(year = 2018, position = "President", regional_aggregation = "Municipality", state = "RJ") %>%
   filter(NUM_TURNO == 2) %>% as_tibble()

municipios <- read_municipality(year = 2018, code_muni = 33)
municipios <- municipios %>% left_join(presidente, by = c("code_muni" = "COD_MUN_IBGE"))


municipios <- municipios %>% 
  group_by(code_muni) %>% 
  mutate(
  total = sum(QTDE_VOTOS),
  perc_vote = round((QTDE_VOTOS * 100) / total, 2)
)
```


```{r, message=FALSE, warning=FALSE}
pt <- municipios %>% group_by(code_muni) %>%
  filter(NUMERO_CANDIDATO == 13)

nb <- poly2nb(pt, queen=TRUE, row.names = pt$name_muni) 

lw <- nb2listw(nb, style="W", zero.policy=TRUE) 

pt$lag <- lag.listw(lw, pt$perc_vote)
```


```{r, echo=FALSE, out.width = "400px"}
lag <- tm_shape(pt) + 
  tm_polygons(col = "lag", style = "fisher", title = "media movel")
perc <- tm_shape(pt) + 
  tm_polygons(col = "perc_vote", style = "fisher", title = "% votos")

tmap_arrange(perc, lag, ncol = 2)
```

---

# O Pulo do gato

```{r}
regressao <- lm(pt$lag ~pt$perc_vote)
#summary(regressao)
coef( regressao )
moran.test(pt$perc_vote, lw)
```

---

# O Pulo do gato

```{r, out.width = "400px"}
plot(pt$lag, pt$perc_vote)
abline(regressao, col = "red")
```

---

# Script para essa live

<br><br><br><br><br><br><br>

### .center[`https://gitlab.com/geocastbrasil/liver/-/blob/master/static/werlive13/werlive13.R`]

---

class: inverse, middle, center

#Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

---

#Considerações finais (5 min) `r icon::fa_grin_beam_sweat()`

## Próxima Live (25/08): 

### We R Live 14: Live com professor Jorge Kazuo Yamamoto

```{r, echo=FALSE, message=FALSE, warning=FALSE, paged.print=FALSE, out.width = "600px", fig.align='center'}

knitr::include_graphics("./img/Yamamoto.png")
```

[`r icon::fa_youtube(size = 2)` Link para a live](https://www.youtube.com/watch?v=PBwpCdCcSq4)

---

class: clear

## Maurício Vancine

`r icon::fa_envelope()` [mauricio.vancine@gmail.com](mauricio.vancine@gmail.com)
<br>
`r icon::fa_twitter()` [@mauriciovancine](https://twitter.com/mauriciovancine)
<br>
`r icon::fa_github()` [@mauriciovancine](https://mauriciovancine.netlify.com/)
<br>
`r icon::fa_link()` [mauriciovancine.netlify.com](https://mauriciovancine.netlify.com/)

<br><br>

## Felipe Sodré M. Barros
`r icon::fa_envelope()` [felipe.b4rros@gmail.com](felipe.b4rros@gmail.com)
<br>
`r icon::fa_twitter()` [@FelipeSMBarros](https://twitter.com/FelipeSMBarros)
<br>
`r icon::fa_gitlab()` [@felipe.b4rros](https://gitlab.com/felipe.b4rros")
<br>
`r icon::fa_link()` [Geo Independência ](https://geoind.wordpress.com/)

<br><br>

Slides criados via pacote [xaringan](https://github.com/yihui/xaringan) e tema [Metropolis](https://github.com/pat-s/xaringan-metropolis)