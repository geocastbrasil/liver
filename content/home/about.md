---
title: "Sobre"
image: "werlive_logo.png"
weight: 2
---

Lives de R para análise de dados geoespaciais toda **terça-feira, às 22:00 h**

#### Formato
As lives terão cerca de uma hora de duração aproximadamente, dividida em **15/20 min.** de introdução ao tema/desafio, pacotes a serem utilizados e principais sintaxes, além de comentários gerais; seguida de **30/40 min.** de "mão na massa"; e os **minutos que sobrarem** (se sobrarem) para considerações finais

#### Informações técnicas
Iremos utilizar o [R](https://www.r-project.org/) versão 4.0.x e o [RStudio](https://rstudio.com/) versão 1.3.x

Repositório do GitLab: https://gitlab.com/geocastbrasil/liver

Dúvidas e sugestões: use as issues do repositório do GitLab acima