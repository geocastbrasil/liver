---
date: "2020-08-12T22:00-00:00"
tags:
- r
- remote sensing
title: We R Live Coding 02 - On fire!
---

{{<youtube 5BipR4L-n1Q>}}

Vamos dar prosseguimento a um formato de Live Coding de R. 

Dessa vez, vamos ter a ilustre presença do Ecólogo João Giovanelli, mestre e doutor em Zoologia e sócio da Seleção Natural (http://selecaonatural.net/).

O João tem feito análises de queimadas para o município de São José do Rio Pardo/SP:

https://www.facebook.com/joao.giovanelli/posts/10224455418117170

http://www.minhasaojose.com.br/ecologo-destaca-que-queimadas-ja-consumiram-cerca-de-15-do-territorio-rio-pardense/?fbclid=IwAR0GXWHaWo4zN-WBzjh1AP6PJS5sNblQOzAD7j6Bd0TFC7eHkWDaTaZqbfA

Nossa intenção será usar o R para fazer as mesmas análises, usando imagens Landsat8 e avançando com o NBR: Normalized Burn Ratio.

Repositório: https://gitlab.com/geocastbrasil/we-r-live-coding

Vídeo: https://youtu.be/5BipR4L-n1Q