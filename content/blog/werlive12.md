---
date: "2020-08-18T22:00-23:00"
tags:
- r
title: We R Live 12 - Geografia das Eleições
---

{{<youtube KN4kkvKkb_w>}}

Nessa live, vamos fazer uma breve pausa na abordagem técnica do R, para escutar do Lucas Gelape (Doutorando em Ciências Políticas pela USP) mias a respeito da "Geografia das Eleições".

A ideia da live é trazer quem entende do assunto para apresentar uma abordagem científica a respeito e aproveitar a temática para a próxima live, onde aprenderemos a respeito de correlação esapcial, usando dados de eleições.

O Lucas Gelape é Cientista Político, doutorando em Ciência Política pela USP. Mains infos: http://lgelape.github.io

Vídeo: https://youtu.be/KN4kkvKkb_w

Slides: https://lgelape.github.io/blog/geografia_eleicoes/geografia_eleitoral_slides.pdf

Descrição: https://lgelape.github.io/blog/geografia_eleicoes/