---
date: "2020-08-25T10:00-11:00"
tags:
- r
- geoestatistica
title: We R Live 14 - ESPECIAL - Geoestatística com Jorge Kazuo Yamamoto
---

{{<youtube aIdntosdKIo>}}

Nessa live especial teremos a honra de receber o professor Jorge Kazuo Yamamoto, geólogo, autor do livro "Geoestatística", junto ao professor Paulo M. Barbosa Landin, e está por lançar seu livro atualizado com scripts em R.

Não perca essa live! Vamos conhecer o autor e sua obra e, quem sabe, sugar um pouco do seu conhecimento?

Slides: [slides](https://werlive.netlify.app/werlive14/apresentacaoJKY_25082020.pdf)

Vídeo: https://youtu.be/aIdntosdKIo