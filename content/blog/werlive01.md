---
date: "2020-04-28T22:00-23:00"
tags:
- r
- rspatial
- rstudio
- tutorial
title: We R Live 01 - Por onde começaR?
--- 

{{<youtube ZORFVdwtJ1U>}}

Primeira live de R, onde iremos explorar as potencialidades do R para análises espaciais.
Começaremos bem do início, com a instalação do R, RStudio, origem da linguagem e principais fontes estudo. 

Slides: https://werlive.netlify.app/werlive01/werlive01

Vídeo: https://youtu.be/ZORFVdwtJ1U

